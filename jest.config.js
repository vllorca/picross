module.exports = {
  preset: '@vue/cli-plugin-unit-jest',
  automock: false,
  setupFiles: [
    './tests/unit/setupJest.js'
  ]
}
