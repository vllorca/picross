import Vue from 'vue'
import VueRouter from 'vue-router'

import Home from '../views/Home.vue'
import Nonogram from '../views/Nonogram.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited. chargement asynchrone de /about (à la demande)
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/nonogram',
    name: 'Nonogram',
    component: Nonogram
  },
  {
    path: '/player',
    name: 'Player',
    component: () => import(/* webpackChunkName: "player" */ '../views/Player.vue')
  }

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
